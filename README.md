# Pokedex 2.0 VueJs

A VueJs reproduction of this pokedex : https://www.pokemon.com/us/pokedex/ fetching pokeAPI.co done with the VueJs CLI

# Test and deploy

Just clone the repository on your machine, get to the "master" branch and execute "npm run serve" to start the Pokedex after dl the node modules :
->git clone https://gitlab.com/jason.moret/pokedex-2.0-vuejs
->git checkout master
->npm i
->npm run serve

The development server is now running on port 8080

